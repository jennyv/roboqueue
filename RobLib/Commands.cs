﻿using NKH.MindSqualls;

namespace RobLib
{
    public class Forward : ICommand
    {
        private readonly NxtMotorSync _pair;

        public Forward(NxtMotorSync pair)
        {
            _pair = pair;
        }

        public void Execute()
        {
            _pair.Run(Robot.RunPower, 0, 0);
        }

        public override string ToString() => ("Go Forward!");
    }

    public class Backward : ICommand
    {
        private readonly NxtMotorSync _pair;

        public Backward(NxtMotorSync pair)
        {
            _pair = pair;
        }

        public void Execute()
        {
            _pair.Run((sbyte) (-1 * Robot.RunPower), 0, 0);
        }

        public override string ToString() => ("Go Backward!");
    }

    public class Left : ICommand
    {
        private readonly NxtMotorSync _pair;

        public Left(NxtMotorSync pair)
        {
            _pair = pair;
        }

        public void Execute()
        {
            _pair.Run(Robot.TurnPower, 0, -1 * 100);
        }

        public override string ToString() => ("Turn Left!");
    }

    public class Right : ICommand
    {
        private readonly NxtMotorSync _pair;

        public Right(NxtMotorSync pair)
        {
            _pair = pair;
        }

        public void Execute()
        {
            _pair.Run(Robot.TurnPower, 0, 100);
        }

        public override string ToString() => ("Turn Right!");
    }
}