﻿namespace RobLib
{
    public interface ICommand
    {
        void Execute();

        string ToString();
    }
}