﻿using System;
using NKH.MindSqualls;

namespace RobLib
{
    public class Robot
    {
        public static sbyte RunPower = 75;
        public static sbyte TurnPower = 50;
        private readonly NxtBrick _brick;
        public NxtMotorSync Pair;

        public Robot(byte port)
        {
            _brick = new NxtBrick(NxtCommLinkType.Bluetooth, port)
            {
                MotorB = new NxtMotor(),
                MotorC = new NxtMotor()
            };
            Pair = new NxtMotorSync(_brick.MotorB, _brick.MotorC);
        }

        public void Connect()
        {
            try
            {
                _brick.Connect();
            }
            catch (Exception)
            {
                throw new Exception("Connection is terminated");
            }
        }

        public void Disconnect()
        {
            _brick.Disconnect();
        }
    }
}