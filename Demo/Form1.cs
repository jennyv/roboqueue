﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Demo
{
    public partial class Form1 : Form
    {
        //public static SortedDictionary<int, ICommand> Queue = new SortedDictionary<int, ICommand>();
        public static SortedDictionary<int, char> Queue = new SortedDictionary<int, char>();

        private readonly Mutex _change = new Mutex();
        private readonly int[] _relation = new int[100];
        private int _curInd = 0;
        private int _lastPr = 0;

        public Form1()
        {
            InitializeComponent(); /*
            if (!string.IsNullOrEmpty(PortNumber.Text = ""))
            {
                MessageBox.Show("Inkorrect number of Port!");
                return;
            }
            try
            {
                _brick = new NxtBrick(NxtCommLinkType.Bluetooth, Convert.ToByte(PortNumber.Text))
                {
                    MotorB = new NxtMotor(),
                    MotorC = new NxtMotor()
                };
                _pair = new NxtMotorSync(_brick.MotorB, _brick.MotorC);
                _brick.Connect();
            }
            catch (Exception)
            {
                MessageBox.Show("Connection is terminated");
            }*/
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            _change.WaitOne();
            if (commands.SelectedItem != null)
            {
                if (_relation[commands.SelectedIndex] == _curInd)
                    return;
                var ind = commands.SelectedIndex;
                Queue.Remove(_relation[ind]);
                RecountRelations(ind);
                commands.Items.RemoveAt(ind);
            }
            _change.ReleaseMutex();
            commands.Refresh();
        }

        private void Run()
        {
            while (Queue.Count > 0)
            {
                var tmp = Queue.First();
                _change.WaitOne();
                _curInd = tmp.Key;
                Queue.Remove(tmp.Key);
                RecountRelations(0);
                commands.Items.RemoveAt(0);
                _change.ReleaseMutex();

                File.AppendAllText(@"C:\Users\Яна\Desktop\RoboQueue\source", tmp.Value.ToString());
                commands.Refresh();
                Thread.Sleep(1000*3);
            }
        }

        private void Start_Click(object sender, EventArgs e)
        {
            Thread.Sleep(1000);
            var runThread = new Thread(Run) {IsBackground = true};
            runThread.Start();
        }

        private void Forward_Click(object sender, EventArgs e)
        {
            _change.WaitOne();
            Queue.Add(_lastPr, 'F');
            commands.Items.Add("F");
            _relation[commands.Items.Count - 1] = _lastPr;
            _change.ReleaseMutex();
            ++_lastPr;
            commands.Refresh();
        }

        private void Backward_Click(object sender, EventArgs e)
        {
            _change.WaitOne();
            Queue.Add(_lastPr, 'B');
            commands.Items.Add("B");
            _relation[commands.Items.Count - 1] = _lastPr;
            _change.ReleaseMutex();
            ++_lastPr;
            commands.Refresh();
        }

        private void Right_Click(object sender, EventArgs e)
        {
            _change.WaitOne();
            Queue.Add(_lastPr, 'R');
            commands.Items.Add("R");
            _relation[commands.Items.Count - 1] = _lastPr;
            _change.ReleaseMutex();
            ++_lastPr;
            commands.Refresh();
        }

        private void Left_Click(object sender, EventArgs e)
        {
            _change.WaitOne();
            Queue.Add(_lastPr, 'L');
            commands.Items.Add("L");
            _relation[commands.Items.Count - 1] = _lastPr;
            _change.ReleaseMutex();
            ++_lastPr;
            commands.Refresh();
        }

        private void Plus_Click(object sender, EventArgs e)
        {
            _change.WaitOne();
            if (commands.SelectedItem != null && commands.SelectedIndex > 0 && _relation[commands.SelectedIndex] != _curInd)
            {
                if (_relation[commands.SelectedIndex] == _curInd)
                    return;
                var ind = commands.SelectedIndex;
                var tmp = Queue[_relation[ind]];
                Queue[_relation[ind]] = Queue[_relation[ind - 1]];
                Queue[_relation[ind - 1]] = tmp;
                commands.Items[ind] = commands.Items[ind - 1];
                commands.Items[ind - 1] = tmp.ToString();
            }
            _change.ReleaseMutex();
            commands.Refresh();
        }

        private void Minus_Click(object sender, EventArgs e)
        {
            _change.WaitOne();
            if (commands.SelectedItem != null && commands.SelectedIndex < commands.Items.Count - 1 && _relation[commands.SelectedIndex] != _curInd)
            {
                if (_relation[commands.SelectedIndex] == _curInd)
                    return;
                var ind = commands.SelectedIndex;
                var tmp = Queue[_relation[ind]];
                Queue[_relation[ind]] = Queue[_relation[ind + 1]];
                Queue[_relation[ind + 1]] = tmp;
                commands.Items[ind] = commands.Items[ind + 1];
                commands.Items[ind + 1] = tmp.ToString();
            }
            _change.ReleaseMutex();
            commands.Refresh();
        }

        private void RecountRelations(int ind)
        {
            var border = commands.Items.Count;
            for (var i = ind; i < border - 1; ++i)
                _relation[i]++;
            _relation[border - 1] = 0;
        }

        private void Form1_Closed(object sender, EventArgs e)
        {
            //_brick.Disconnect();
        }
    }
}