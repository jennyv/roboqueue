﻿namespace Demo
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.commands = new System.Windows.Forms.ListBox();
            this.Forward = new System.Windows.Forms.Button();
            this.Left = new System.Windows.Forms.Button();
            this.Right = new System.Windows.Forms.Button();
            this.Backward = new System.Windows.Forms.Button();
            this.Delete = new System.Windows.Forms.Button();
            this.Start = new System.Windows.Forms.Button();
            this.PortNumber = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Plus = new System.Windows.Forms.Button();
            this.Minus = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // commands
            // 
            this.commands.FormattingEnabled = true;
            this.commands.Location = new System.Drawing.Point(50, 12);
            this.commands.MultiColumn = true;
            this.commands.Name = "commands";
            this.commands.Size = new System.Drawing.Size(128, 225);
            this.commands.TabIndex = 0;
            // 
            // Forward
            // 
            this.Forward.Image = ((System.Drawing.Image)(resources.GetObject("Forward.Image")));
            this.Forward.Location = new System.Drawing.Point(231, 12);
            this.Forward.Name = "Forward";
            this.Forward.Size = new System.Drawing.Size(30, 30);
            this.Forward.TabIndex = 1;
            this.Forward.UseVisualStyleBackColor = true;
            this.Forward.Click += new System.EventHandler(this.Forward_Click);
            // 
            // Left
            // 
            this.Left.Image = ((System.Drawing.Image)(resources.GetObject("Left.Image")));
            this.Left.Location = new System.Drawing.Point(194, 50);
            this.Left.Name = "Left";
            this.Left.Size = new System.Drawing.Size(30, 30);
            this.Left.TabIndex = 2;
            this.Left.UseVisualStyleBackColor = true;
            this.Left.Click += new System.EventHandler(this.Left_Click);
            // 
            // Right
            // 
            this.Right.Image = ((System.Drawing.Image)(resources.GetObject("Right.Image")));
            this.Right.Location = new System.Drawing.Point(267, 50);
            this.Right.Name = "Right";
            this.Right.Size = new System.Drawing.Size(30, 30);
            this.Right.TabIndex = 3;
            this.Right.UseVisualStyleBackColor = true;
            this.Right.Click += new System.EventHandler(this.Right_Click);
            // 
            // Backward
            // 
            this.Backward.Image = ((System.Drawing.Image)(resources.GetObject("Backward.Image")));
            this.Backward.Location = new System.Drawing.Point(231, 86);
            this.Backward.Name = "Backward";
            this.Backward.Size = new System.Drawing.Size(30, 30);
            this.Backward.TabIndex = 4;
            this.Backward.UseVisualStyleBackColor = true;
            this.Backward.Click += new System.EventHandler(this.Backward_Click);
            // 
            // Delete
            // 
            this.Delete.Image = ((System.Drawing.Image)(resources.GetObject("Delete.Image")));
            this.Delete.Location = new System.Drawing.Point(12, 158);
            this.Delete.Name = "Delete";
            this.Delete.Size = new System.Drawing.Size(30, 30);
            this.Delete.TabIndex = 5;
            this.Delete.UseVisualStyleBackColor = true;
            this.Delete.Click += new System.EventHandler(this.Delete_Click);
            // 
            // Start
            // 
            this.Start.Image = ((System.Drawing.Image)(resources.GetObject("Start.Image")));
            this.Start.Location = new System.Drawing.Point(199, 209);
            this.Start.Name = "Start";
            this.Start.Size = new System.Drawing.Size(99, 30);
            this.Start.TabIndex = 6;
            this.Start.UseVisualStyleBackColor = true;
            this.Start.Click += new System.EventHandler(this.Start_Click);
            // 
            // PortNumber
            // 
            this.PortNumber.Location = new System.Drawing.Point(267, 183);
            this.PortNumber.Name = "PortNumber";
            this.PortNumber.Size = new System.Drawing.Size(31, 20);
            this.PortNumber.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(196, 190);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Port number";
            // 
            // Plus
            // 
            this.Plus.Image = ((System.Drawing.Image)(resources.GetObject("Plus.Image")));
            this.Plus.Location = new System.Drawing.Point(14, 86);
            this.Plus.Name = "Plus";
            this.Plus.Size = new System.Drawing.Size(30, 30);
            this.Plus.TabIndex = 9;
            this.Plus.UseVisualStyleBackColor = true;
            this.Plus.Click += new System.EventHandler(this.Plus_Click);
            // 
            // Minus
            // 
            this.Minus.Image = ((System.Drawing.Image)(resources.GetObject("Minus.Image")));
            this.Minus.Location = new System.Drawing.Point(14, 122);
            this.Minus.Name = "Minus";
            this.Minus.Size = new System.Drawing.Size(30, 30);
            this.Minus.TabIndex = 10;
            this.Minus.UseVisualStyleBackColor = true;
            this.Minus.Click += new System.EventHandler(this.Minus_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(317, 257);
            this.Controls.Add(this.Minus);
            this.Controls.Add(this.Plus);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.PortNumber);
            this.Controls.Add(this.Start);
            this.Controls.Add(this.Delete);
            this.Controls.Add(this.Backward);
            this.Controls.Add(this.Right);
            this.Controls.Add(this.Left);
            this.Controls.Add(this.Forward);
            this.Controls.Add(this.commands);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "RoboQueue";
            this.Load += new System.EventHandler(this.Form1_Closed);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox commands;
        private System.Windows.Forms.Button Forward;
        private System.Windows.Forms.Button Left;
        private System.Windows.Forms.Button Right;
        private System.Windows.Forms.Button Backward;
        private System.Windows.Forms.Button Delete;
        private System.Windows.Forms.Button Start;
        private System.Windows.Forms.TextBox PortNumber;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Plus;
        private System.Windows.Forms.Button Minus;
    }
}

