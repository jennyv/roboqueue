﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using RobLib;

namespace Demo
{
    public partial class MainForm : Form
    {
        public static SortedDictionary<int, ICommand> Queue = new SortedDictionary<int, ICommand>();
        private readonly Mutex _change = new Mutex();
        private readonly int[] _relation = new int[100];
        private static readonly byte Port = 5;
        private readonly Robot _robo = new Robot(Port);
        private int _curInd;
        private int _lastPr;

        public MainForm()
        {
            InitializeComponent();
            PortNumber.Text = Port.ToString();
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            _change.WaitOne();
            if (commands.SelectedItem != null)
            {
                if (_relation[commands.SelectedIndex] == _curInd)
                    return;
                var ind = commands.SelectedIndex;
                Queue.Remove(_relation[ind]);
                RecountRelations(ind);
                commands.Items.RemoveAt(ind);
            }
            _change.ReleaseMutex();
            commands.Refresh();
        }

        private void Run()
        {
            while (Queue.Count > 0)
            {
                var tmp = Queue.First();
                _change.WaitOne();
                _curInd = tmp.Key;
                Queue.Remove(tmp.Key);
                RecountRelations(0);
                commands.Items.RemoveAt(0);
                _change.ReleaseMutex();

                tmp.Value.Execute();
                commands.Refresh();
                Thread.Sleep(2 * 1000);
                _robo.Pair.Brake();
            }
            _robo.Pair.Idle();
        }

        private void Start_Click(object sender, EventArgs e)
        {
            Thread.Sleep(1000);
            try
            {
                _robo.Connect();
                var runThread = new Thread(Run) {IsBackground = true};
                runThread.Start();
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }
        }

        private void Forward_Click(object sender, EventArgs e)
        {
            AddCommand(new Forward(_robo.Pair));
        }

        private void Backward_Click(object sender, EventArgs e)
        {
            AddCommand(new Backward(_robo.Pair));
        }

        private void Right_Click(object sender, EventArgs e)
        {
            AddCommand(new Right(_robo.Pair));
        }

        private void Left_Click(object sender, EventArgs e)
        {
            AddCommand(new Left(_robo.Pair));
        }

        private void Plus_Click(object sender, EventArgs e)
        {
            _change.WaitOne();
            if (commands.SelectedItem != null && commands.SelectedIndex > 1 &&
                _relation[commands.SelectedIndex] != _curInd)
            {
                if (_relation[commands.SelectedIndex] == _curInd)
                    return;
                var ind = commands.SelectedIndex;
                var tmp = Queue[_relation[ind]];
                Queue[_relation[ind]] = Queue[_relation[ind - 1]];
                Queue[_relation[ind - 1]] = tmp;
                commands.Items[ind] = commands.Items[ind - 1];
                commands.Items[ind - 1] = tmp.ToString();
            }
            _change.ReleaseMutex();
            commands.Refresh();
        }

        private void Minus_Click(object sender, EventArgs e)
        {
            _change.WaitOne();
            if (commands.SelectedItem != null && commands.SelectedIndex < commands.Items.Count - 1 &&
                _relation[commands.SelectedIndex] != _curInd)
            {
                if (_relation[commands.SelectedIndex] == _curInd)
                    return;
                var ind = commands.SelectedIndex;
                var tmp = Queue[_relation[ind]];
                Queue[_relation[ind]] = Queue[_relation[ind + 1]];
                Queue[_relation[ind + 1]] = tmp;
                commands.Items[ind] = commands.Items[ind + 1];
                commands.Items[ind + 1] = tmp.ToString();
            }
            _change.ReleaseMutex();
            commands.Refresh();
        }

        private void RecountRelations(int ind)
        {
            var border = commands.Items.Count;
            for (var i = ind; i < border - 1; ++i)
                _relation[i]++;
            _relation[border - 1] = 0;
        }

        private void Form1_Closed(object sender, EventArgs e)
        {
            _robo.Disconnect();
        }

        private void AddCommand(ICommand cmd)
        {
            _change.WaitOne();
            Queue.Add(_lastPr, cmd);
            commands.Items.Add(cmd.ToString());
            _relation[commands.Items.Count - 1] = _lastPr;
            _change.ReleaseMutex();
            ++_lastPr;
            commands.Refresh();
        }
    }
}